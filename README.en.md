# LargeIntegerAdditionAndMultiplication

#### Description
C/++ implementation of large integer addition and multiplication, arbitrary conversion of 2-10 base numbers

#### Instructions

1.  Open main.cpp and copy the corresponding function code and add it to your own project file

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
